# Web Artisans Laravel-FPM Image #

Base image for Laravel applications being deployed with PHP-FPM. This image does not include a web server. We recommend using it in conjunction with our [Node & Laravel Pod Proxy for Kubernetes](https://bitbucket.org/web-artisans/nginx-node-laravel) image.